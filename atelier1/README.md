# Atelier 1 : installation de PostgreSQL et première base simple

Vérifier que la version 12 est bien candidate à l'installation :

~~~~
$ apt policy postgresql
postgresql:
  Installed: 12+213.pgdg90+1
  Candidate: 12+213.pgdg90+1
  Version table:
 *** 12+213.pgdg90+1 500
        500 http://apt.postgresql.org/pub/repos/apt stretch-pgdg/main amd64 Packages
        100 /var/lib/dpkg/status
...
~~~~

À la fin de l'installation tout semble ok :
~~~~
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.UTF-8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

fixing permissions on existing directory /var/lib/postgresql/12/main ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Etc/UTC
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... ok

Success. You can now start the database server using:

    pg_ctlcluster 12 main start
~~~~

En attandant des lectures sur Pg et les bases de données relationnelles :

https://twobithistory.org/2017/12/29/codd-relational-model.html

https://en.wikipedia.org/wiki/Edgar_F._Codd

https://fr.wikipedia.org/wiki/PostgreSQL
https://en.wikipedia.org/wiki/PostgreSQL

La théorie mathématique :

https://fr.wikipedia.org/wiki/Alg%C3%A8bre_relationnelle

Le blog Use the Index Luke: https://use-the-index-luke.com/

Une bonne illustration du positionnement des différents SGBDR :

https://modern-sql.com/slides

Plus spécifiquement sur PostgreSQL :
https://modern-sql.com/blog/2018-02/standard-sql-features-where-postgresql-beats-its-competitors

Un article du blog particulièrement intéressant concernant MySQL vs Pg:

https://use-the-index-luke.com/blog/2016-07-29/on-ubers-choice-of-databases

## Première connexion en utilisateur postgres

L'installation sous UNIX crée un compte système posgresql. Par défaut (sous UNIX du moins)
considère qu'à chaque utilisateur système correspond un _rôle_ PostgreSQL ainsi qu'une
base de donnée du même nom. Il faut exécuter le client `psql` sous cette identité
pour accéder au seveur en tant qu'administrateur. De là on peut créer les rôles et
bases de données pour les utilisateurs. Sur notre système le compte utilisateur est
`admin` vous devez adapter à votre configuration.

Sous Windows je ne sais pas trop... on va voir.

~~~~
admin@ip-10-0-0-50:~$ sudo su postgres
postgres@ip-10-0-0-50:/home/admin$ psql 
psql (12.2 (Debian 12.2-2.pgdg90+1))
Type "help" for help.

postgres=# \db
       List of tablespaces
    Name    |  Owner   | Location 
------------+----------+----------
 pg_default | postgres | 
 pg_global  | postgres | 
(2 rows)

postgres=#\q
~~~~

Les autres commandes d'administration de PostgreSQL doivent, du moins pour l'instant,
être lancé sous cette même identité `postgres`, par exemple pour créer un rôle 
correspondant à l'utilisateur système `admin` on fait :

~~~~
postgres@ip-10-0-0-50:/home/admin$ createuser --interactive
Enter name of role to add: admin
Shall the new role be a superuser? (y/n) y
~~~~

## Travaillons en tant qu'utilisateur "normal"

Maintenant nous pouvons créer une base à notre nom (c'est la base sur laquelle le
client psql tente de se connecter par défaut) :

~~~~ 
postgres@ip-10-0-0-50:/home/admin$ exit
admin@ip-10-0-0-50:~$ whoami
admin
admin@ip-10-0-0-50:~$ createdb admin
admin@ip-10-0-0-50:~$ psql
psql (12.2 (Debian 12.2-2.pgdg90+1))
Type "help" for help.

admin=# 
admin=# \conninfo
You are connected to database "admin" as user "admin" via socket in "/var/run/postgresql" at port "5432".
~~~~

~~~~
admin=# CREATE TABLE test(id int not null, nom varchar(15), prenom varchar(15)) ;
CREATE TABLE
admin=# INSERT INTO test(id, nom, prenom) VALUES(1,'John','Doe');
INSERT 0 1
admin=# INSERT INTO test(nom, prenom) VALUES('John', 'Cleese');
ERROR:  null value in column "id" violates not-null constraint
DETAIL:  Failing row contains (null, John, Cleese).
admin=# SELECT * FROM test;
 id | nom  | prenom 
----+------+--------
  1 | John | Doe
(1 row)
~~~~

SQL fait même calculatrice (marche pas sous Oracle!)

~~~~
admin=# SELECT 41 + 1 AS hello;
 hello 
-------
    42
(1 row)
~~~~

## Création d'une autre base et conception d'un schéma

La base va s'appeler `acme` et va contenir deux tables `employes` et `departement` avec
les champs (on ne met pas de contraintes sur les champs pour l'instant à part NOT NULL).

- table employes
  * id : INT NOT NULL
  * nom : TEXT
  * prenom : TEXT
  * titre (i.e. poste occupé) : TEXT
  * departement (id du département de l'employé) : INT
  * date de naissance : DATE
  * salaire : NUMERIC(8,2)


- table departement
  * id : INT NOT NULL
  * nom : TEXT 
  * manager (id de l'employé qui EST le manager) : INT 

Ce qui correspond aux requètes SQL suivantes (fichier `employes1.sql`) :

~~~~SQL
CREATE TABLE IF NOT EXISTS employes(
        id INT NOT NULL,
        nom TEXT,
        prenom TEXT,
        departement INT,
        date_de_naissance DATE,
        salaire NUMERIC(6,2)
);

CREATE TABLE IF NOT EXISTS departements(
        id INT NOT NULL,
        nom TEXT,
        manager INT
);
~~~~

Qui peut être exécuté à partir du client `psql` ainsi :

~~~~
admin=# \i employes1.sql
CREATE TABLE
CREATE TABLE
~~~~

On peut examiner le schema avec `\d` :

~~~~
admin=# \dt
           List of relations
 Schema |     Name     | Type  | Owner 
--------+--------------+-------+-------
 public | departements | table | admin
 public | employes     | table | admin
 public | test         | table | admin
(3 rows)

admin=# \d employes
                      Table "public.employes"
      Column       |     Type     | Collation | Nullable | Default 
-------------------+--------------+-----------+----------+---------
 id                | integer      |           | not null | 
 nom               | text         |           |          | 
 prenom            | text         |           |          | 
 departement       | integer      |           |          | 
 date_de_naissance | date         |           |          | 
 salaire           | numeric(6,2) |           |          | 

admin=# \d departements
            Table "public.departements"
 Column  |  Type   | Collation | Nullable | Default 
---------+---------+-----------+----------+---------
 id      | integer |           | not null | 
 nom     | text    |           |          | 
 manager | integer |           |          | 
~~~~

# Importer des données CSV

Doc: https://www.postgresqltutorial.com/import-csv-file-into-posgresql-table/

L'import se fait avec la commanide "pseudo sql" COPY:

~~~~
admin=# COPY departements(id,nom,manager) FROM '/home/admin/departement1_import.csv' DELIMITER ',' CSV;
COPY 3
admin=# select * from departements ;
 id |     nom      | manager 
----+--------------+---------
  1 | Comptabilité |       3
  2 | Ventes       |       5
  3 | Assistance   |       6
admin=# COPY employes(id,nom,prenom,departement,date_de_naissance,salaire) FROM '/home/admin/employe1_import.csv' DELIMITER ',' CSV;
COPY 10
admin=# \q
~~~~

Remarquez que le fichier est lu par le serveur, s'il n'est pas possible d'y déposer le fichier
on peut utiliser `COPY FROM PROGRAM 'wget -q -O- http[s]://url/vers/le/fichier'` et placer
le fichier sur un serveur Web accessible par le serveur PostgreSQL.

# La question des clés primaires et étrangères

La notion de clé recouvre trois choses distinctes :

- Une clé primaire est une contrainte sur un ou plusieurs champ qui les impose uniques (collectivement)
  dans une table donnée
  => contrainte PRIMARY KEY
- Une clé étrangère est une contrainte sur un champ qui impose que sa valeur corresponde à
  un enregistrement présent dans une autre table

Pour les clés primaires on souhaite généralement qu'elles soient automatiquement générées.
  => type SERIAL (principalement)

- Serial :   https://www.postgresqltutorial.com/postgresql-serial/
- Clés primaires : https://www.postgresqltutorial.com/postgresql-primary-key/
- Clés étrangères : https://www.postgresqltutorial.com/postgresql-foreign-key/

Comment améliorer notre schéma pour implémenter ces contraintes et l'auto-génération de clefs ?

Pour le clefs primaires uniquement dans un premier temps, en faisant en sorte que le fichier
détruise les tables si elles existent déjà:

~~~~SQL
DROP TABLE IF EXISTS employes;
CREATE TABLE IF NOT EXISTS employes(
        id SERIAL PRIMARY KEY,
        nom TEXT,
        prenom TEXT,
        departement INT,
        date_de_naissance DATE,
        salaire NUMERIC(8,2)
);

DROP TABLE IF EXISTS departements;
CREATE TABLE IF NOT EXISTS departements(
        id SERIAL,
        nom TEXT,
        manager INT,
        PRIMARY KEY(id)
);
~~~~

Qui se joue comme d'habitude :

~~~~
admin=# \i employes2.sql
DROP TABLE
CREATE TABLE
DROP TABLE
CREATE TABLE
admin=# \q
~~~~

## Définition des clefs primaire

On peut ajouter la déclaration du champ departement dans la table employes comme clé 
étrangère référençant le champs id de la table departement :

~~~~SQL
DROP TABLE IF EXISTS departements CASCADE;
CREATE TABLE IF NOT EXISTS departements(
        id SERIAL,
        nom TEXT,
        manager INT,
        PRIMARY KEY(id)
);

DROP TABLE IF EXISTS employes;
CREATE TABLE IF NOT EXISTS employes(
        id SERIAL PRIMARY KEY,
        nom TEXT,
        prenom TEXT,
        departement INT REFERENCES departements(id),
        date_de_naissance DATE,
        salaire NUMERIC(8,2)
);
~~~~

En faisant les choses dans l'ordre (et en demandant au premier DROP TABLE de supprimer en
cascade), on peut alors insérer les données :

~~~~
$ psql
psql (12.2 (Debian 12.2-2.pgdg90+1))
Type "help" for help.

admin=# \i employes3.sql                                                                                     psql:employes3.sql:1: NOTICE:  drop cascades to constraint employes_departement_fkey on table employes
DROP TABLE
CREATE TABLE
DROP TABLE
CREATE TABLE
admin=# \i copy_from_csv2.sql 
COPY 3
COPY 10

~~~~

Seule une contrainte de clef étrangère a été posée. En poser une autre (sur l'existence comme employé 
d'un manager) n'est pas possible dans le CREATE TABLE (la table employes n'existant pas encore),
on peut ajouter à posteriori la contrainte avec ALTER TABLE.

## Quelques requètes SELECT pour se (re) faire la main :

Concevez et tester les requètes SELECT pour obtenir :

- La liste des employés du département comptabilité

~~~~SQL
SELECT * FROM departements, employes WHERE departements.id = employes.departement AND departements.nom = 'Comptabilité';

SELECT * FROM employes WHERE employes.departement = 1;
~~~~

- La liste des employés qui gagnent plus de 50000

~~~~SQL
SELECT nom || ' ' || prenom AS employe FROM employes WHERE employes.salaire >= 50000;
~~~~

- La liste des employés classé par age

~~~~SQL
SELECT nom, date_de_naissance FROM employes ORDER BY date_de_naissance ASC;
~~~~

- L'effectif de chaque département

~~~~SQL
SELECT d.nom, COUNT(*) AS effectif FROM departements d JOIN employes e ON d.id = e.departement GROUP BY d.id ORDER BY d.nom ASC;
~~~~

- La valeur moyenne des salaires de chaque département

~~~~SQL
SELECT d.nom, AVG(salaire) AS moyenne_salaire FROM departements d JOIN employes e ON d.id = e.departement GROUP BY d.id ORDER BY moyenne_salaire ASC;
~~~~

- Pour chaque employé (nom et prénom) qui est son manager (nom et prénom)

~~~~SQL
SELECT e.nom, e.prenom, c.nom, c.prenom FROM employes AS e, departements as d, employes as c WHERE e.departement = d.id AND d.manager = c.id;

SELECT e.nom || ' ' ||  e.prenom as employe, c.nom || ' ' || c.prenom as manager FROM employes AS e JOIN  departements as d ON e.departement = d.id JOIN employes as c ON d.manager = c.id WHERE e.id <> c.id ;
~~~~

## Un mot sur l'analyse et l'optimisation de requètes

PostgreSQL peut vous montrer sa stratégie d'exécution de requète : en préfixant la requête avec
EXPLAIN

Quels index avons-nous déjà sur nos tables ?

~~~~SQL
SELECT
    tablename,
    indexname,
    indexdef
FROM
    pg_indexes
WHERE
    schemaname = 'public'
ORDER BY
    tablename,
    indexname;
~~~~

- Utilisez EXPLAIN sur une requête qui fait des jointures (comme les deux dernières)
- Ajoutez des index sur les clefs étrangères de nos tables:

~~~~SQL
CREATE INDEX departement_manager_idx ON departements(manager);
...
~~~~

Comparez les résultat de EXPLAIN avant et après.
