<?php

require 'vendor/autoload.php';

use PostgreSQLTutorial\Connection as Connection;

try {
	Connection::get()->connect();
	echo 'A connection to the PostgreSQL database server has been established successfully.';
} catch (\PDOException $e) {
	echo $e->getMessage();
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>PostgreSQL PHP Querying Data Demo</title>
    <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
</head>
<body>
<div class="container">
    <h1>Atelier 2</h1>
    <div>
        <h4>Obtenir la liste des instructeurs d'un cours donné : Python</h4>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Cours</th>
                <th>Noms Instructeurs</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Python</td>
                <td>
					<?php try {
						// connect to the PostgreSQL database
						$pdo = Connection::get()->connect();
						//
						$sql = "SELECT c.titre, i.nom, i.prenom "
							. "FROM instructeurs i, cours c, enseigne e "
							. "WHERE e.instructeurs_id = i.id "
							. "AND e.cours_id = c.id "
							. "AND c.titre = 'Python'";
						$rset = $pdo->prepare( $sql );
						$rset->execute();
						while ($row = $rset->fetch( PDO::FETCH_ASSOC )) {
							print($row['nom'] . '<br>');
						}
					} catch (\PDOException $e) {
						echo $e->getMessage();
					}
					?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <h4>Obtenir la liste des cours enseignés par un instructeur donné : Poulier</h4>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Instructeur</th>
                <th>Titres Cours</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Poulier</td>
                <td>
					<?php try {
						// connect to the PostgreSQL database
						$pdo = Connection::get()->connect();
						//
						$sql = "SELECT c.titre "
							. "FROM instructeurs i, cours c, enseigne e "
							. "WHERE e.instructeurs_id = i.id "
							. "AND e.cours_id = c.id "
							. "AND i.nom = 'Poulier'";
						$rset = $pdo->prepare( $sql );
						$rset->execute();
						while ($row = $rset->fetch( PDO::FETCH_ASSOC )) {
							print($row['titre'] . '<br>');
						}
					} catch (\PDOException $e) {
						echo $e->getMessage();
					}
					?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <h4>Obtenir pour chaque cours le nombre total d'inscrits à un instant t</h4>
        <p>
            Essayez divers type de jointures interne et externe
            <br>
            Dans quels cas les résultat peuvent différer ?
        </p>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Cours à l'instant t :</th>
                <th>Nombre d'inscrits</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td></td>
                <td>
					<?php try {
						// connect to the PostgreSQL database
						$pdo = Connection::get()->connect();
						//
						$sql = "SELECT titre, SUM(nombre_inscrits) AS total_inscrits "
							. "FROM cours,sessions "
							. "WHERE cours.id = sessions.cours "
							. "GROUP BY cours.id";
						$rset = $pdo->prepare( $sql );
						$rset->execute();
						while ($row = $rset->fetch( PDO::FETCH_ASSOC )) {
							print($row['total_inscrits'] . '<br>');
						}
					} catch (\PDOException $e) {
						echo $e->getMessage();
					}
					?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <h4>Calculer et renvoyer pour chaque cours le nombre total d'inscrits avec deux stratégies une jointure interne
            et une externe gauche sur cours,</h4>
        <p>
            Les résultats diffèrent-il ? Pourquoi ? Quelle la "bonne" requête ?
        </p>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Cours</th>
                <th>Nombre d'inscrits</th>
            </tr>
            </thead>
            <tbody>
            <tr>

            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <div>
            <h4>Association de type Python <-> PostgreSQL par le connecteur psycopg2 (moi en php)</h4>
            <h6>Date</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select date " . "from test" );

					echo "Type du champ date : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
            <h6>Array</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select pay_by_day " . "from test2" );

					echo "Type du champ array : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
            <h6>Point</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select point " . "from test3" );

					echo "Type du champ point : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
            <h6>Bit string</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select a, b " . "from test4" );

					echo "Type du champ Bit string : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
        </div>
        <div>
            <h6>Test des valeurs python</h6>
            <div style="width:100%;height:auto">
            <textarea style="width:100%">
                    <class 'int'> -> INT -> <class 'int'>
                    int -> INT -> int

                    <class 'float'> -> DECIMAL(8,2) -> <class 'decimal.Decimal'>
                    float -> DECIMAL(8,2) -> Decimal

                    <class 'str'> -> DATE -> <class 'datetime.date'>
                    str -> DATE -> date
                </textarea>
            </div>
        </div>
    </div>
    <div>
        <div>
            <h4>Test de plusieurs types Python(moi php) <-> PostgreSQL</h4>
            <h6>Array avec text[][]</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select schedule " . "from test5" );

					echo "Type du champ array[] : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
            <h6>Array avec int[3][3]</h6>
            <p>
				<?php
				try {
					$pdo = "host=localhost port=5434 dbname=CentreFormation user=Justine password=02maya02";
					$conn = pg_connect( $pdo );

					// On assume que 'titre' est un type varchar
					$res = pg_query( $conn, "select squares " . "from test6" );

					echo "Type du champ array[][] : ", pg_field_type( $res, 0 );
				} catch (\PDOException $e) {
					echo $e->getMessage();
				}
				?>
            </p>
        </div>
    </div>
</div>
</body>
</html>