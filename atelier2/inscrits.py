#!/usr/bin/env python3

import psycopg2


def get_sessions(code, cursor):
    sql = "SELECT code, titre, nb_inscrits " \
          "FROM cours JOIN sessions ON cours.id_cours = sessions.id_cours " \
          "WHERE cours.code = %s "

    cursor.execute(sql, (code,) )
    return [ (code,titre,nb) for code,titre,nb in cursor ]

if __name__ == '__main__':
    import sys
    usage = "{} code".format(sys.argv[0])
    if len(sys.argv) != 2:
        print(usage)
        exit(1)
    c = psycopg2.connect("",database='formations')
    #print(c.get_dsn_parameters(),"\n")
    cur = c.cursor()
    _, code = sys.argv

    print(get_sessions(code,cursor=cur))
