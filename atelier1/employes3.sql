DROP TABLE IF EXISTS departements CASCADE;
CREATE TABLE IF NOT EXISTS departements(
        id SERIAL,
        nom TEXT,
        manager INT,
	PRIMARY KEY(id)
);

DROP TABLE IF EXISTS employes;
CREATE TABLE IF NOT EXISTS employes(
        id SERIAL PRIMARY KEY,
        nom TEXT,
        prenom TEXT,
        departement INT REFERENCES departements(id) DEFERRABLE,
        date_de_naissance DATE,
        salaire NUMERIC(8,2)
);

