#!/usr/bin/env python3

import psycopg2

#c = psycopg2.connect("")
c = psycopg2.connect("",database='formations')

cur = c.cursor()

print(c.get_dsn_parameters(),"\n")

cur.execute("SELECT version();")

r = cur.fetchone()

print(r[0])

cur.execute("SELECT code,titre FROM cours;")

for code,titre in cur:
    print("{} : {}".format(code,titre))
