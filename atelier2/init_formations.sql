COPY cours(code,titre,duree) FROM '/home/admin/cours-posgresql-1/atelier2/cours.csv' DELIMITER ',' CSV;
COPY instructeurs(nom,prenom) FROM '/home/admin/cours-posgresql-1/atelier2/instructeurs.csv' DELIMITER ',' CSV;
COPY enseigne(id_instructeur,id_cours) FROM '/home/admin/cours-posgresql-1/atelier2/enseigne.csv' DELIMITER ',' CSV;
COPY sessions(id_cours,date,nb_inscrits,id_instructeur) FROM '/home/admin/cours-posgresql-1/atelier2/sessions.csv' DELIMITER ',' CSV;
