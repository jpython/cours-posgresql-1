# Conception de bases de données

## Représenter l'activité d'un centre de formation

Concevoir une base représentant l'activité d'un centre de formation. Les
notions clés (entités) envisagées sont :

- Cours : identifiant (i.e. PGSQL01,PYTH02, etc.), titre, description, durée en jours
- Sessions : cours, date, nombre d'inscrits
- Instructeurs : nom, prénom

On souhaite aussi représenter la relation "enseigne" entre Cours et Instructeurs.

Définir un schéma de base de données et implémenter les requètes CREATE TABLE correspondantes.

Peupler la base avec des fichiers CSV (trois ou quatre cours, quelques sessions, quelques
instructeurs).

Concevoir et tester des requètes pour:

- Obtenir la liste des instructeurs d'un cours donné
- Obtenir la liste des cours enseignés par un instructeur donné
- Obtenir pour chaque cours le nombre total d'inscrits à un instant t
  * Essayez divers type de jointures interne et exter~ne
  * Dans quels cas les résultat peuvent différer ? 

- Calculer et renvoyer pour chaque cours le nombre total d'inscrits avec
  deux stratégies une jointure interne et une externe gauche sur cours, les
  résultats diffèrent-il ? Pourquoi ? Quelle la "bonne" requête ?

~~~~
    SELECT code, titre, SUM(nb_inscrits) as total_inscrits
    FROM cours JOIN sessions
         ON cours.id_cours = sessions.id_cours
    GROUP BY cours.id_cours
~~~~

ou 

~~~~
    SELECT code, titre, SUM(nb_inscrits) AS total_inscrits
    FROM cours,sessions
    WHERE cours.id_cours = sessions.id_cours
    GROUP BY cours.id_cours
~~~~

sont des jointures internes.

Une jointure externe gauche est :

~~~~
    SELECT code, titre, SUM(nb_inscrits) AS total_inscrits
    FROM cours LEFT JOIN sessions
         ON cours.id_cours = sessions.id_cours
    GROUP BY cours.id_cours
~~~~

Vous pouvez récupérer les solutions proposées et (en adaptant les chemins d'accès
vers les fichiers csv) initialiser la base et la remplir :

~~~~
(venv) admin@ip-10-0-0-50:~/cours-posgresql-1/atelier2$ psql -d formations
psql (12.2 (Debian 12.2-2.pgdg90+1))
Type "help" for help.

formations=# \i formation.sql
DROP INDEX
DROP INDEX
DROP INDEX
DROP INDEX
psql:formation.sql:6: NOTICE:  drop cascades to 2 other objects
DETAIL:  drop cascades to constraint sessions_id_cours_fkey on table sessions
drop cascades to constraint enseigne_id_cours_fkey on table enseigne
DROP TABLE
CREATE TABLE
psql:formation.sql:14: NOTICE:  drop cascades to 2 other objects
DETAIL:  drop cascades to constraint sessions_id_instructeur_fkey on table sessions
drop cascades to constraint enseigne_id_instructeur_fkey on table enseigne
DROP TABLE
CREATE TABLE
DROP TABLE
CREATE TABLE
DROP TABLE
CREATE TABLE
CREATE INDEX
CREATE INDEX
CREATE INDEX
CREATE INDEX
formations=# \i init_formations.sql
COPY 8
COPY 5
COPY 7
COPY 5
~~~~

# Types de PostgreSQL

Certainement un des points sur lequel PostgreSQL est le plus riche comparablement
à son alter égo MySQL/MariaDB et aux SGBDR propriétaires.

https://www.postgresql.org/docs/12/datatype.html

- Numériques, entiers, décimaux (BCD), flottants
- Chaînes et texte
- Dates, instants, intervalles de temps
- Adresses IP, mac, ...
- Géométrie
- Tableaux
- Structures (composites)
- XML, JSON
- ...
- plus encore avec des extensioins comme PostGIS


# Connecteurs Python (et PHP ?)

Si vous voulez utiliser PHP au lieu de Python c'est ok, mais je n'aurai
pas de corrigé à fournir.

- le connecteur "état de l'art" pour PostgreSQL est psycopg2
- Écrire une fonction qui prend un code de cours en arguments et renvoie
  la liste des tuples (date, nb_inscrits)
- Une fois testée écrivez un site Web Flask qui renvoie ces informations
  sur une page Web à partir de l'url http://hote:port/code
  (sans doute http://hote:port/session?code=XXX en PHP)

Note: l'installation d'un venv puis dans cet environnement de flask
et psycopg2 nécessite quelques installations, sur une Debian-like:

~~~~Bash
$ sudo apt install python3-venv
$ sudo apt install build-essential
$ sudo apt install python3-dev
$ sudo apt install libpq-dev

$ python3 -m venv venv # création du venv
$ source venv/bin/activate # activation du venv

(venv)...$ pip install flask psycopg2
~~~~

## Test simple de connection avec psycopg2

Voici test.py:

~~~~Python
#!/usr/bin/env python3

import psycopg2

#c = psycopg2.connect("")
c = psycopg2.connect("",database='formations')

cur = c.cursor()

print(c.get_dsn_parameters(),"\n")

cur.execute("SELECT version();")

r = cur.fetchone()

print(r[0])

cur.execute("SELECT code,titre FROM cours;")

for code,titre in cur:
    print("{} : {}".format(code,titre))
~~~~

et son exécution :

~~~~
(venv) admin@ip-10-0-0-50:~/cours-posgresql-1/atelier2$ ./test.py
{'tty': '', 'options': '', 'gssencmode': 'prefer', 'target_session_attrs': 'any', 'dbname': 'formations', 'krbsrvname': 'postgres', 'sslmode': 'prefer', 'passfile': '/home/admin/.pgpass', 'sslcompression': '0', 'user': 'admin', 'port': '5432'}

PostgreSQL 12.2 (Debian 12.2-2.pgdg90+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516, 64-bit
PGSQL01 : Développement avec PostgreSQL
PYTH02 : Développement Objet avec Python 3
PYTH01 : Introduction au langage Python
LINX01 : Utilisation de GNU/Linux
LINX02 : Administration de GNU/Linux
DOCK01 : Mise en œuvre de conteneurs Docker
K8S01 : Mise en œuvre de Kubernetes
WIN02 : Administration MS Windows Serveur et AD
~~~~

## Association de type Python <-> PostgreSQL par le connecteur psycopg2

Écrire un script Python (ou équivalent dans un autre langage avec un
connecteur PostgreSQL disponible) qui crée un dictionnaire (hash)
dont les clef sont des noms de types PostgreSQL et les valeurs une
donnée d'un type compatible. 

~~~~Python

dataDict = { 'INT':42, 'DECIMAL(8,2)':3.14, ... }

~~~~

Parcourez ce dictionnaire, pour chaque entrée :

- Exécutez une requète créant une table avec un seul champ du type SQL concerné
- Insérez la donnée correspondante
- Exécutez une requète SELECT pour lire la donnée dans la table
- Affichez le type Python de la valeur reçue

Nous aurons ainsi la possibilité d'examiner pour chaque type PostgreSQL à
quel type Python psychopg2 l'associe. Étendez le dictionnaire Python pour
examiner comment sont géré les dates, les tableaux, d'autre types SQL plus
exotiques sont traités.

## Création d'un lexique MySQL-MariaDB / PostgreSQL

Pour chaque fonctionnalité que nous avons couvert si il y a une différence
de syntaxe, de fonctionnalité, etc. entre MySQL (et ses diverses variantes)
et PostgreSQL décrire les différence.

Ex: création d'une clé primaire auto incrémentée, conversion d'un résultat
NULL en une valeur par défaut, gestion des contraintes, etc.

Quels sont les licences des deux produits ? Quelle différence cela peut-il
faire pour la distribution d'un logiciel dérivé de l'un ou l'autre (en 
particulier considérez la question de la licence de la bibliothèque
_cliente_ sur laquelle se basent les connecteurs Python, C, PHP, etc.)

| Fonctionnalité       | MySQL/MariaDB           | PostgreSQL    | Remarques      |
|--------------------- |-------------------------|---------------|----------------|
| Licence serveur      | GPL ou commercial       | MIT           |                |
| Licence client       | GPL ou commercial       | MIT           |                |
| Clés auto incr       | INT AUTO\_INCREMENT     | SERIAL        |                |
| Créer une base       | CREATE DATABASE         | createdb      | command line   | 
| Voir le schéma       | SHOW TABLES             | \dt           |                | 
| Voir le schéma       | DESCRIBE nom\_table     | \d nom\_table |                | 
| Changer de base      | USE base                | \c base       |                | 
| Contraintes CHECK    | Ok mais ignorées        | OUI           |                |
| Gestion NULL         | IFNULL                  | COALESCE      |                |
| Transactions         | oui (si non autocommit) | OUI           |                |  
| Conformité SQL       | Partielle               | Totale        |                |
| Extensions (ex. GIS) | Limitées                | Nombreuses    |                |
| NoSQL (hstore,json)  | Limité                  | OUI           |                |
| Lang. procéduraux    | Limité                  | PgSQL, autres |                |
| Cluster              | Oui (simple)            | Oui (complexe)|                |
| Analyse de requêtes  | Très basique            | Avancée       |                |

Sources: 

Oracle (arf) : https://www.oracle.com/fr/database/postgresql-versus-mysql.html
2ndQuadrant : https://www.2ndquadrant.com/fr/postgresql/postgresql-vs-mysql/ 
(note: entreprise spécialisé sur l'offre PostgreSQL)

Plus objectif:

https://www.guru99.com/postgresql-vs-mysql-difference.html
https://www.xplenty.com/blog/postgresql-vs-mysql-which-one-is-better-for-your-use-case/
https://people.apache.org/~jim/NewArchitect/webtech/2001/09/jepson/index.html

La licence de la libmysqlclient est GPL (sauf si vous avez opter une des licences
commerciales proposé à vil prix par Oracle), il est donc possible de déployer en
interne une application sans contrainte, c'est de la distribuer qui peut être 
problématique car elle est liée à la libmysqlclient (connecteur C, Python, PHP,
Perl, etc.). Si elle est en GPL pas de souci, si vous souhaitez appliquer une
autre licence c'est impossible, en particulier une licence propriétaire.

Des projets existent pour redévelopper une libmysqlclient sous une licence
plus ouverte (LGPL ou BSD) mais ils avancent peu et ne sont pas soutenus
par Oracle (et ne le seront jamais). À noter que le même problème se pose
pour MariaDB.

PostgreSQL n'a pas ce problème : la licence MIT (BSD-like) permet de distribuer
des produits dérivées sous n'importe quel licence. Il en existe de nombreux
embarquant libpg tout autant que le serveur PostgreSQL complet avec des
licences différentes, y compris propriétaires. Seul inconvénient, à mon
goût, il existe des variantes du serveur PostgreSQL non-libres qui ajoutent
par exemple des modèles de réplications différents.


# Préparer un backend PostgreSQL pour ShareCode :

Vous vous souvenez de https://framagit.org/jpython/share-code ?

Vous pouvez cloner le projet et y ajouter un backend PostgreSQL
en lieu et place du backend actuel (fichiers)

- Encore mieux si on peut choisir avec une option de conf (dans
  le script : backend=... ou fichier .ini de conf)
- On souhaite enregistrer en base plusieurs information (pas
  toutes utilisées encore) :
  - le contenu du code partagé
  - le langage de programmation (créer une table auxiliaire languages)
  - les infos sur l'utilisateur qui a déposé le code : date, adresse IP,
    navigateur utilisé, ... à des fins d'un éventuel filtrage anti abus
    (l'objet request transmis par Flask contient toutes les infos)

Note: l'objet request initialisé par Flask contient l'ensemble des
informations intéressantes. Cf. https://tedboy.github.io/flask/generated/generated/flask.Request.html

~~~~
CREATE TABLE languages(id SERIAL NOT NULL PRIMARY KEY, name TEXT)

CREATE TABLE codes(id SERIAL NOT NULL PRIMARY KEY,
     language INT REFERENCES languages(id),
     creator REFERENCES clients(id),
     last_write REFERENCES clients(id),
     uid VARCHAR(10), code TEXT)

CREATE TABLE clients(id SERIAL NOT NULL PRIMARY KEY, 
                     remote_addr INET, 
                     user_agent TEXT,
                     long FLOAT, lat FLOAT,
                     fqdn TEXT,
                     timestamp TIMESTAMP)
~~~~

TODO: implémenter en Python.
 
~~~~
